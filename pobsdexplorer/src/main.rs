extern crate pobsdlib;
use pobsdlib::collections::DataBase;
use std::{env, io, path, process};

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        eprintln!("Not enough arguments");
        process::exit(1);
    }
    if args.len() > 2 {
        eprintln!("Too many arguments");
        process::exit(1);
    }
    let path = path::Path::new(&args[1]);
    if path.is_file() {
        let db_game = DataBase::new(&args[1]);
        loop {
            println!("Search:");
            let mut search = String::new();
            io::stdin()
                .read_line(&mut search)
                .expect("Failed to read command");
            let search_terms: Vec<&str> = search.split(" ").collect();
            if search_terms.len() != 2 {
                println!("Read the doc!");
                continue;
            }
            let result = db_game
                .games
                .get_item_with_field(search_terms[0], search_terms[1].trim());
            for game in result.items {
                println!(
                    "============================================================================"
                );
                println!("{}", game.name);
            }
        }
    } else {
        eprintln!("This is not a file");
    }
}
