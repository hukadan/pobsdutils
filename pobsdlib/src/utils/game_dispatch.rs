use crate::collections::DataBase;
use crate::models::{Field, Game};

pub fn game_dispatch(field: Field, database: &mut DataBase) {
    match field {
        Field::Game(name) => {
            let mut game = Game::default();
            let game_id = database.games.len() + 1;
            game.name = name.to_string();
            game.id = game_id;
            database.games.insert(game_id, game);
        }
        Field::Cover(name) => {
            let last_game_id = database.games.len();
            if let Some(game) = database.games.get_mut(&last_game_id) {
                game.cover = name.to_string();
            };
        }
        Field::Engine(name) => {
            let last_game_id = database.games.len();
            if let Some(game) = database.games.get_mut(&last_game_id) {
                game.engine = name.to_string();
            };
            database
                .engines
                .entry(name.to_string())
                .and_modify(|e| e.push(last_game_id))
                .or_insert(vec![last_game_id]);
        }
        Field::Setup(name) => {
            let last_game_id = database.games.len();
            if let Some(game) = database.games.get_mut(&last_game_id) {
                game.setup = name.to_string();
            };
        }
        Field::Runtime(name) => {
            let last_game_id = database.games.len();
            if let Some(game) = database.games.get_mut(&last_game_id) {
                game.runtime = name.to_string();
            };
            database
                .runtimes
                .entry(name.to_string())
                .and_modify(|e| e.push(last_game_id))
                .or_insert(vec![last_game_id]);
        }
        Field::Hints(name) => {
            let last_game_id = database.games.len();
            if let Some(game) = database.games.get_mut(&last_game_id) {
                game.hints = name.to_string();
            };
        }
        Field::Dev(name) => {
            let last_game_id = database.games.len();
            if let Some(game) = database.games.get_mut(&last_game_id) {
                game.publi = name.to_string();
            };
            database
                .devs
                .entry(name.to_string())
                .and_modify(|e| e.push(last_game_id))
                .or_insert(vec![last_game_id]);
        }
        Field::Publi(name) => {
            let last_game_id = database.games.len();
            if let Some(game) = database.games.get_mut(&last_game_id) {
                game.dev = name.to_string();
            };
            database
                .publis
                .entry(name.to_string())
                .and_modify(|e| e.push(last_game_id))
                .or_insert(vec![last_game_id]);
        }
        Field::Version(name) => {
            let last_game_id = database.games.len();
            if let Some(game) = database.games.get_mut(&last_game_id) {
                game.version = name.to_string();
            };
        }
        Field::Status(name) => {
            let last_game_id = database.games.len();
            if let Some(game) = database.games.get_mut(&last_game_id) {
                game.status = name.to_string();
            };
        }
        Field::Store(items) => {
            let last_game_id = database.games.len();
            if let Some(game) = database.games.get_mut(&last_game_id) {
                for item in items {
                    game.store.push(item.to_string());
                }
            };
        }
        Field::Genres(items) => {
            let last_game_id = database.games.len();
            if let Some(game) = database.games.get_mut(&last_game_id) {
                for item in &items {
                    game.genres.push(item.to_string());
                }
            };
            for item in &items {
                database
                    .genres
                    .entry(item.to_string())
                    .and_modify(|e| e.push(last_game_id))
                    .or_insert(vec![last_game_id]);
            }
        }
        Field::Tags(items) => {
            let last_game_id = database.games.len();
            if let Some(game) = database.games.get_mut(&last_game_id) {
                for item in &items {
                    game.tags.push(item.to_string());
                }
            };
            for item in &items {
                database
                    .tags
                    .entry(item.to_string())
                    .and_modify(|e| e.push(last_game_id))
                    .or_insert(vec![last_game_id]);
            }
        }
        Field::Year(year) => {
            let last_game_id = database.games.len();
            if let Some(game) = database.games.get_mut(&last_game_id) {
                game.year = year.to_string();
            };
            database
                .years
                .entry(year.to_string())
                .and_modify(|e| e.push(last_game_id))
                .or_insert(vec![last_game_id]);
        }
        Field::Unknown(field) => {
            eprintln!("Skipping unknown field: {}", field);
        }
    };
}
