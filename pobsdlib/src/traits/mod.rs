pub use crate::traits::basic_item::BasicItem;
pub use crate::traits::game_item::GameItem;

pub mod basic_item;
pub mod game_item;
