#[macro_use] extern crate rocket;
extern crate pobsdlib;

use rocket::State;
use pobsdlib::collections::DataBase;
use std::{env, io, path, process};

#[get("/game/<id>")]
fn game_id(db: &State<DataBase>, id: usize) -> String {
    serde_json::to_string_pretty(db.games.get_by_id(id).expect("Should not fail")).unwrap()
}

#[get("/game/name/<name>")]
fn game_name(db: &State<DataBase>, name: String) -> String {
    serde_json::to_string_pretty(db.games.get_by_name(&name).expect("Should not fail")).unwrap()
}

#[get("/game/tag/<tag>")]
fn game_tag(db: &State<DataBase>, tag: String) -> String {
    serde_json::to_string_pretty(&db.games.filter_with_tag(&tag)).unwrap()
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .manage(DataBase::new("../db/openbsd-games.db"))
        .mount("/", routes![game_id, game_name, game_tag])
}

