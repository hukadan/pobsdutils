table! {
    games (id) {
        id -> Integer,
        name -> Nullable<Text>,
        cover -> Nullable<Text>,
        setup -> Nullable<Text>,
        version -> Nullable<Text>,
        status -> Nullable<Text>,
    }
}
