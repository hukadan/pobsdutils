#[macro_use]
extern crate diesel;
extern crate dotenv;
extern crate pobsdlib;


use diesel::prelude::*;
use diesel::sqlite::SqliteConnection;
use dotenv::dotenv;
use pobsdlib::collections::DataBase;
use std::{env, path, process};

fn main() {
    dotenv().ok();
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        eprintln!("Not enough arguments");
        process::exit(1);
    }
    if args.len() > 2 {
        eprintln!("Too many arguments");
        process::exit(1);
    }
    let path = path::Path::new(&args[1]);
    if path.is_file() {
        let db_game = DataBase::new(&args[1]);
    } else {
        eprintln!("This is not a file");
    }
    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    SqliteConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url));
}


